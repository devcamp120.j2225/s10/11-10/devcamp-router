import Footer from "../components/footer/Footer";
import Header from "../components/header/Header";
import FirstLayout from "../components/firstpage/FirstPage";

const FirstPage = () => {
    return (
        <>
            <Header />

            <FirstLayout />

            <Footer />
        </>
    )
}

export default FirstPage;