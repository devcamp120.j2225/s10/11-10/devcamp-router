import Footer from "../components/footer/Footer";
import Header from "../components/header/Header";
import Home from "../components/home/Home";

const HomePage = () => {
    return (
        <>
            <Header />

            <Home />

            <Footer />
        </>
    )
}

export default HomePage;