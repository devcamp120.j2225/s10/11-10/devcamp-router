import Footer from "../components/footer/Footer";
import Header from "../components/header/Header";
import ThirdLayout from "../components/thirdpage/ThirdPage";

const ThirdPage = () => {
    return (
        <>
            <Header />

            <ThirdLayout />

            <Footer />
        </>
    )
}

export default ThirdPage;