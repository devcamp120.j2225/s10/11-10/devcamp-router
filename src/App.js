import { Route, Routes } from "react-router-dom";
import FirstPage from "./pages/FirstPage";
import HomePage from "./pages/HomePage";
import SecondPage from "./pages/SecondPage";
import ThirdPage from "./pages/ThirdPage";

function App() {
  return (
    <div>
      <Routes>
        <Route exact path="/" element={<HomePage />}></Route>
        <Route exact path="/firstpage" element={<FirstPage />}></Route>
        <Route exact path="/secondpage/:param" element={<SecondPage />}></Route>
        <Route exact path="/thirdpage" element={<ThirdPage />}></Route>
        <Route path="*" element={<HomePage />}></Route>
      </Routes>
    </div>
  );
}

export default App;
