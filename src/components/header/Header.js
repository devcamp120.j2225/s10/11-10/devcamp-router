const Header = () => {
    return (
        <div style={{padding: "20px"}}>
            Header

            <hr/>
                <ul style={{listStyleType: "none"}}>
                    <li><a href="/">Home</a></li>
                    <li><a href="/firstpage">Firstpage</a></li>
                    <li><a href="/secondpage/1234">Secondpage param 1234</a></li>
                    <li><a href="/secondpage/third">Secondpage param third</a></li>
                    <li><a href="/thirdpage">Thirdpage</a></li>
                    <li><a href="/abcpage">Otherpage</a></li>
                </ul>
            <hr/>
        </div>
    )
}

export default Header;