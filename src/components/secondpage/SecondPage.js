import { useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";

const SecondPage = () => {
    const navigate = useNavigate();

    const { param } = useParams();

    useEffect(() => {
        if(param === "third") {
            navigate("/thirdpage");
        }
    }, [])

    return (
        <div style={{padding: "20px"}}>
            SecondPage:
            Param = {param}
        </div>
    )
}

export default SecondPage;